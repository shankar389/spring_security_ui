import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../service/authentication.service';
import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';

@Component({
  templateUrl: './home.component.html'
})
export class HomeComponent {

  title = 'Demo';
  greeting = {};

  constructor(private app: AuthenticationService, private http: HttpClient) {
    http.get('http://192.168.0.26:9000/token').subscribe(data => {
      const token = data['token'];
      http.get('http://192.168.0.26:9000', {headers : new HttpHeaders().set('X-Auth-Token', token)})
        .subscribe(response => this.greeting = response);
    });
  }

  authenticated() { return this.app.authenticated; }

}
