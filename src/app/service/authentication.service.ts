import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable()
export class AuthenticationService {

  authenticated = false;

  constructor(private http: HttpClient) {
  }

  // Authentication method
  authenticate(credentials, callback) {
    if (credentials.username) {
        const headers = new HttpHeaders(credentials ? {
            authorization : 'Basic ' + btoa(credentials.username + ':' + credentials.password),
            accept  : 'application/json'
        } : {});

        this.http.get('http://192.168.0.26:9000/user', {headers: headers}).subscribe(response => {
            if (response['name']) {
                this.authenticated = true;
            } else {
                this.authenticated = false;
            }
            return callback && callback();
        },
        error => {
            console.log(error);
        }
    );
    }

    }

}
